package com.atlassian.jira.rest.client.internal.json.gen;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.input.ProjectCategoryInput;

/**
 * Convert {@link ProjectCategoryInput} objects into the JSON representation.
 * 
 * @see VES-366
 * @author ckl
 * @since 2.0-m0
 */
public class ProjectCategoryInputJsonGenerator implements JsonGenerator<ProjectCategoryInput> {
	@Override
	public JSONObject generate(ProjectCategoryInput issue) throws JSONException {
		final JSONObject jsonObject = new JSONObject();

		jsonObject.put("name", issue.getName());
		jsonObject.put("description", issue.getDescription());

		return jsonObject;
	}
}
