package com.atlassian.jira.rest.client.api;

import java.net.URI;

import com.atlassian.jira.rest.client.api.domain.ProjectCategory;
import com.atlassian.jira.rest.client.api.domain.input.ProjectCategoryInput;
import com.atlassian.util.concurrent.Promise;

/**
 * @author sebastian[dot]weinert[at]neos-it[dot]de
 */
public interface ProjectCategoryRestClient {

	Promise<ProjectCategory> getProjectCategory(Long id);

	Promise<ProjectCategory> getProjectCategory(URI projectCategoryUri);

	Promise<Iterable<ProjectCategory>> getAllProjectCategories();

	Promise<ProjectCategory> createProjectCategory(final ProjectCategoryInput issue);

	Promise<ProjectCategory> updateProjectCategory(Long id, final ProjectCategoryInput projectCategory);
	
	/**
	 * Remove a project category
	 * 
	 * @param projectCategoryUri
	 * @return
	 */
	Promise<Void> removeProjectCategory(URI projectCategoryUri);
}
