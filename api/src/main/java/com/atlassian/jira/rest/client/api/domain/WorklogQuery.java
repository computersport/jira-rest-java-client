package com.atlassian.jira.rest.client.api.domain;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import com.google.common.base.Objects;

/**
 * @author christopher[dot]klein[at]neos-it[dot]de
 * @since 3.0.2
 */
public class WorklogQuery {
	private long id;
	private DateTime startDate;
	private String issueKey;
	private String userId;
	private int duration;
	@Nullable
	private DateTime updateDate;
	@Nullable
	private String comment;

	public WorklogQuery(long id, DateTime startDate, String issueKey, String userId, int duration,
			@Nullable DateTime updateDate, @Nullable String comment) {

		this.id = id;
		this.startDate = startDate;
		this.issueKey = issueKey;
		this.userId = userId;
		this.duration = duration;
		this.updateDate = updateDate;
		this.comment = comment;
	}

	public long getId() {
		return id;
	}

	public DateTime getStartDate() {
		return startDate;
	}

	public String getIssueKey() {
		return issueKey;
	}

	public String getUserId() {
		return userId;
	}

	public int getDuration() {
		return duration;
	}

	public DateTime getUpdateDate() {
		return updateDate;
	}

	public String getComment() {
		return comment;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("startDate", startDate).add("issueKey", issueKey)
				.add("userId", userId).add("duration", duration).add("updateDate", updateDate).add("comment", comment)
				.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WorklogQuery) {
			WorklogQuery that = (WorklogQuery) obj;
			return Objects.equal(this.id, that.id) && Objects.equal(this.startDate, that.startDate)
					&& Objects.equal(this.issueKey, that.issueKey) && Objects.equal(this.userId, that.userId)
					&& Objects.equal(this.duration, that.duration) && Objects.equal(this.updateDate, that.updateDate);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id, startDate, issueKey, userId, duration, updateDate, comment);
	}
}
