package com.atlassian.jira.rest.client.api;

import javax.annotation.Nullable;

import com.atlassian.jira.rest.client.api.domain.WorklogQuery;
import com.atlassian.util.concurrent.Promise;

/**
 * Reads worklog from the JIRA endpoint provided by
 * <a href="https://github.com/everit-org/jira-worklog-query-plugin">jira-
 * worklog-query-plugin</a>.
 * 
 * @author christopher[dot]klein[at]neos-it[dot]de
 * @since 3.0.2
 */
public interface WorklogQueryRestClient {
	/**
	 * Find worklogs in a given interval
	 * 
	 * @param startDate
	 *            not null, format YYYY-MM-DD; The start date from which the
	 *            worklogs can be viewed. The query is run from the beginning of
	 *            the given day (00:00:00)
	 * @param endDate
	 *            optional, format YYYY-MM-DD; The end date until which the
	 *            worklogs can be viewed. The query is run until the end of the
	 *            given day (23:59:59). The default value is the current date.
	 * @param user
	 *            optional (one of "user" or "group"); A JIRA user name whose
	 *            worklogs are to be listed.
	 * @param group
	 *            optional (one of "user" or "group"); A JIRA group whose
	 *            worklogs are to be listed.
	 * @param project
	 *            string, optional; The key parameter of a JIRA project. Unless
	 *            it is defined, all projects are queried, otherwise only the
	 *            ones under the defined project.
	 * @param fields
	 *            nullable/optional; A list of additional fields to return.
	 *            Available fields are: comment, updated.
	 */
	Promise<Iterable<WorklogQuery>> findWorklogs(String startDate, @Nullable String endDate, String user, String group,
			@Nullable String project, @Nullable String[] fields);

	/**
	 * Create a new instance with given API endpoint version of
	 * jira-worklog-query-plugin. If the current instance already uses this
	 * version, the current instance is returned
	 * 
	 * @param version
	 *            not null
	 * @return not null
	 */
	WorklogQueryRestClient inVersion(String version);
}
